<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_balance'))
{
    function get_balance($var = '')
    {
    	$CI =& get_instance();
    	$CI->load->model('Common_Model'); 
    	$con['conditions'] = array('id' => $CI->session->userdata('user_id'));
    	$con['returnType'] = 'single';
        $balance = $CI->Common_Model->getRows($con, 'db_users');
        return $balance['account_balance'] == '' ? 0 : $balance['account_balance'];
    }   

    function get_new_balance($var = '')
    {
        $CI =& get_instance();
        $CI->load->model('Common_Model'); 
        $con['conditions'] = array('id' => $CI->session->userdata('user_id'));
        $con['returnType'] = 'single';
        $balance = $CI->Common_Model->getRows($con, 'db_users');
        return $balance['new_account_balance'] == '' ? 0 : $balance['new_account_balance'];
    } 

    function get_clients() {
    	$CI =& get_instance();
    	$CI->load->model('Common_Model'); 
    	$con['conditions'] = array('status' => '1', 'role_id' => 1);
        return $CI->Common_Model->getRows($con, 'db_users');
    }
}