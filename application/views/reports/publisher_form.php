<?php $this->load->view('common/header.php'); 
 ?>
 <style type="text/css">
 .form-group {
  margin: 0px 10px;
  display: block !important;
 }
 .form-group button {
  margin-top: 14px;
 }
 </style>

          <div class="content-wrap">
          <?php  $this->load->view('common/messages.php'); ?>
        <div class="main">
            <div class="container-fluid">
                 <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 p-r-0 title-margin-right">
              <div class="page-header">
                <div class="page-title">
                  <h1>Hello, <span>Welcome Here</span></h1>
                </div>
              </div>
            </div>
            <!-- /# column -->
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 p-l-0 title-margin-left">
              <div class="page-header page_header_2">
                <div class="page-title">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#"><?= $title; ?></a></li>
                    <li class="breadcrumb-item active">table-row-select</li>
                  </ol>
                </div>
              </div>
            </div>
            <!-- /# column -->
          </div>
                <!-- /# row -->
                <section id="main-content">
                    <div class="row">
                    <div class="col-lg-12">
                      <form class="form-inline" action="<?= base_url(); ?>Reports/publisher">
                        <div class="form-group">
                          <label for="start_date">Start Date:</label>
                          <input type="date" value="<?= isset($_GET['start_date']) ? $_GET['start_date'] : ''; ?>" class="form-control" name="start_date" id="start_date">
                        </div>
                       <div class="form-group">
                          <label for="end_date">End Date:</label>
                          <input type="date" value="<?= isset($_GET['end_date']) ? $_GET['end_date'] : ''; ?>" class="form-control" name="end_date" id="end_date">
                        </div>
                        <div class="form-group">
                          <button type="submit" class="btn btn-default">Generate Report</button>
                        </div>
                        
                      </form>
                    </div>
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="bootstrap-data-table-panel shm">
                                    <div class="table-responsive">
                                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered table_export_wrapper">
                                            <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Total Clicks</th>
                                                    <th>Expired Clicks</th>
                                                    <th>De-duped Clicks</th>
                                                    <th>Value</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                              <?php if ($reports) :
                                                  foreach ($reports as $report) :
                                               ?>
                                                <tr>
                                                    <td><?= $report['date']; ?></td>
                                                    <td><?= $report['unique_clicks']; ?></td>
                                                    <td><?= $report['expired_clicks']; ?></td>
                                                    <td><?= $report['double_clicks']; ?></td>
                                                    <td><?= $report['earning']; ?></td>
                                                </tr>
                                              <?php
                                                endforeach; 
                                               endif; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                 <p>2018 ©  mid Admin Board by <a href="#">webstrot.</a></p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
         <div class="container" id="edit-element"></div>


<!-- end row -->
<?php $this->load->view('common/footer.php'); ?>