<!DOCTYPE html>
<html lang="en">
<head>
  <title>Truusec</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style type="text/css">
   #con_packageName, #con_gemName, #con_group, #con_name {
      display: none;
   }
  </style>
</head>
<body>

<div class="container">
  <h2>Test package</h2>
  <form action="<?= base_url(); ?>Home/call_api">
    <div class="col-md-6" id="con_packageName">
      <div class="form-group">
        <label for="packageName">Package Name:</label>
        <input type="text" class="form-control" id="packageName" value="<?= @$_GET['packageName']; ?>" placeholder="Enter package name" name="packageName">
      </div>
    </div>
    <div class="col-md-6" id="con_gemName">
      <div class="form-group">
        <label for="gemName">Gem Name:</label>
        <input type="text" class="form-control" id="gemName" value="<?= @$_GET['gemName']; ?>" placeholder="Enter gem name" name="gemName">
      </div>
    </div>
    <div class="col-md-6" id="con_group">
      <div class="form-group">
        <label for="group">Group:</label>
        <input type="text" class="form-control" id="group" value="<?= @$_GET['group']; ?>" placeholder="Enter group" name="group">
      </div>
    </div>
    <div class="col-md-6" id="con_name">
      <div class="form-group">
        <label for="name">Name:</label>
        <input type="text" class="form-control" id="name" value="<?= @$_GET['name']; ?>" placeholder="Enter name" name="name">
      </div>
    </div>
    <div class="col-md-6" id="con_groupId">
      <div class="form-group">
        <label for="groupId">Group ID:</label>
        <input type="text" class="form-control" id="groupId" value="<?= @$_GET['groupId']; ?>" placeholder="Enter group id" name="groupId">
      </div>
    </div>
    <div class="col-md-6" id="con_artifactId">
      <div class="form-group">
        <label for="artifactId">Artifact ID:</label>
        <input type="text" class="form-control" id="artifactId" value="<?= @$_GET['artifactId']; ?>" placeholder="Enter artifact id" name="artifactId">
      </div>
    </div>
    <div class="col-md-6" id="con_version">
      <div class="form-group">
        <label for="version">Version:</label>
        <input type="text" class="form-control" value="<?= @$_GET['version']; ?>" id="version" placeholder="Enter version" name="version">
      </div>
    </div>
    <div class="col-md-6" id="con_org">
      <div class="form-group">
        <label for="org">Org:</label>
        <input type="text" value="<?= @$_GET['org']; ?>" class="form-control" id="org" placeholder="Enter org" name="org">
      </div>
    </div>
    <div class="col-md-6" id="con_repository">
      <div class="form-group">
        <label for="repository">Repository:</label>
        <input type="text" value="<?= @$_GET['repository']; ?>" class="form-control" id="repository" placeholder="Enter repository" name="repository">
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label for="fileType">Select File type:</label>
        <select class="form-control" id="fileType" name="fileType">
          <option <?= @$_GET['fileType'] == 'moven' ? 'selected' : ''; ?> value="moven">Moven</option>
          <option <?= @$_GET['fileType'] == 'npm' ? 'selected' : ''; ?> value="npm">NPM</option>
          <option <?= @$_GET['fileType'] == 'golangdep' ? 'selected' : ''; ?> value="golangdep">DEP</option>
          <option <?= @$_GET['fileType'] == 'yarn' ? 'selected' : ''; ?> value="yarn">Yarn</option>
          <option <?= @$_GET['fileType'] == 'rubygems' ? 'selected' : ''; ?> value="rubygems">Rubygems</option>
          <option <?= @$_GET['fileType'] == 'gradle' ? 'selected' : ''; ?> value="gradle">Gradle</option>
          <option <?= @$_GET['fileType'] == 'sbt' ? 'selected' : ''; ?> value="sbt">SBT</option>
          <option <?= @$_GET['fileType'] == 'pip' ? 'selected' : ''; ?> value="pip">PIP</option>
          <option <?= @$_GET['fileType'] == 'composer' ? 'selected' : ''; ?> value="composer">Composer</option>
          <option <?= @$_GET['fileType'] == 'dep-graph' ? 'selected' : ''; ?> value="dep-graph">Dep Graph</option>
        </select>
      </div>
    </div>
    <div class="col-md-12">
    <button type="submit" class="btn btn-default">Check</button>
    </div>
  </form>
  <div class="col-md-12">
  <?php if (isset($data->message)) : ?>
    <h3>Error:</h3>
    <p><?= $data->message; ?></p>
  <?php endif; ?>
  <?php if (isset($data->issues->vulnerabilities)) :
        echo '<h3>Result:</h3>';

        echo '<table border="1"><thead><tr><td>ID</td><td>URL</td><td>Title</td><td>Type</td><td>Description</td></tr></thead><tbody>';
        foreach ($data->issues->vulnerabilities as $issue) :
          echo '<tr><td>'.$issue->id.'</td><td>'.$issue->url.'</td><td>'.$issue->title.'</td><td>'.$issue->type.'</td><td>'.$issue->description.'</td></tr>';
        endforeach;
     endif; ?>
      </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
  $(function() {
    $('#fileType').change(function() {
      let val = $(this).val();
      if (val == 'moven') {
        $('#con_groupId, #con_artifactId, #con_version, #con_org, #con_repository').show();
        $('#con_packageName, #con_gemName, #con_group, #con_name').hide();
      } else if (val == 'npm') {
        $('#con_packageName, #con_version, #con_org').show();
        $('#con_groupId, #con_artifactId, #con_gemName, #con_group, #con_name, #con_repository').hide();
      } else if (val == 'golangdep' || val == 'govendor' || val == 'composer' || val == 'dep-graph' || val == 'yarn') {
        $('#con_org').show();
        $('#con_groupId, #con_artifactId, #con_gemName, #con_group, #con_name, #con_packageName, #con_version, #con_repository').hide();
      } else if (val == 'rubygems') {
        $('#con_gemName, #con_version, #con_org').show();
        $('#con_artifactId, #con_packageName, #con_groupId, #con_group, #con_name, #con_repository').hide();
      } else if (val == 'gradle') {
        $('#con_group, #con_name, #con_version, #con_org, #con_repository').show();
        $('#con_gemName, #con_artifactId, #con_packageName, #con_groupId').hide();
      } else if (val == 'sbt') {
        $('#con_groupId, #con_artifactId, #con_version, #con_org, #con_repository').show();
        $('#con_gemName, #con_name, #con_packageName, #con_group').hide();
      } else if (val == 'pip') {
        $('#con_packageName, #con_version, #con_org').show();
        $('#con_gemName, #con_name, #con_groupId, #con_group, #con_repository, #con_artifactId').hide();
      }
    })
  })
</script>
</body>
</html>
