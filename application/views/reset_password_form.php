<?php include_once('common/login_header.php'); ?>
 <body class="bg-primary">

    <div class="unix-login">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="login-content">
                        <?php include_once('common/messages.php'); ?>
                        <div class="login-logo">
                            <a href="index-2.html"><span>mid</span></a>
                        </div>
                        <div class="login-form">
                            <h4>Change Login Password</h4>
                            <form action="<?= base_url(); ?>Password_Resets/change_pass" method="POST">
                                <input type="hidden" value="<?= $token; ?>" name="token">
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" name="password" class="form-control" placeholder="Password">
                                </div>
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" name="confirm_password" class="form-control" placeholder="Password">
                                </div>
                                <button type="submit" name="changePassword" value="true" class="btn btn-primary btn-flat m-b-30 m-t-30">Change Password</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
<?php include_once('common/login_footer.php'); ?>