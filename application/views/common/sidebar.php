 <div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
      <div class="nano">
        <div class="nano-content">
          <div class="logo"><a href="index-2.html"><img src="assets/images/logo.png" alt="" /></a></div>
          <ul>
            <li class="label">Main</li>
            <?php if ($this->session->userdata('role_id') == 2) : ?>
            <li><a class="sidebar-sub-toggle"><i class="ti-home"></i> Dashboard <span class="sidebar-collapse-icon ti-angle-down"></span></a>
              <ul>
                <li><a href="<?= base_url(); ?>Dashboard">Dashboard</a></li>                
              </ul>
            </li>


            <li><a href="<?= base_url(); ?>Users/"><i class="ti-user"></i> Clients</a></li>

            <li><a class="sidebar-sub-toggle"><i class="ti-list"></i> Transactions <span class="badge badge-primary">2</span> <span class="sidebar-collapse-icon ti-angle-down"></span></a>
              <ul>
                <li><a href="<?= base_url(); ?>Transactions/debits">Debit Transactions</a></li>       
                <li><a href="<?= base_url(); ?>Transactions/credits">Credit Transactions</a></li>
              </ul>
            </li>
            

          <?php endif; ?>

            <li><a href="<?= base_url(); ?>Password/form"><i class="ti-lock"></i> Change Password</a></li>
            <li><a href="<?= base_url(); ?>Login/logout"><i class="ti-close"></i> Logout</a></li>
          </ul>
        </div>
      </div>
    </div>