<!DOCTYPE html>
<html lang="en">


<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

   <title>Mid Dashboard Template</title>

    <!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" type="image/png" href="<?=base_url(); ?>assets/images/favicon.png" />
    <!-- Styles -->
    <link href="<?=base_url(); ?>assets/css/lib/weather-icons.css" rel="stylesheet" />
    <link href="<?=base_url(); ?>assets/css/lib/owl.carousel.min.css" rel="stylesheet" />
    <link href="<?=base_url(); ?>assets/css/lib/owl.theme.default.min.css" rel="stylesheet" />
    <link href="<?=base_url(); ?>assets/css/lib/font-awesome.min.css" rel="stylesheet">
    <link href="<?=base_url(); ?>assets/css/lib/themify-icons.css" rel="stylesheet">
    <link href="<?=base_url(); ?>assets/css/lib/menubar/sidebar.css" rel="stylesheet">
    <link href="<?=base_url(); ?>assets/css/lib/bootstrap.min.css" rel="stylesheet">

    <link href="<?=base_url(); ?>assets/css/lib/helper.css" rel="stylesheet">
    <link href="<?=base_url(); ?>assets/css/style.css" rel="stylesheet">
    <script src="<?= base_url(); ?>assets/js/lib/jquery.min.js"></script>

</head>
<body>

<?php include_once('sidebar.php'); ?>
<?php include_once('top-menu.php'); ?>

