    <!-- jquery vendor -->

    <script src="<?= base_url(); ?>assets/js/lib/jquery.nanoscroller.min.js"></script>
    <!-- nano scroller -->
    <script src="<?= base_url(); ?>assets/js/lib/menubar/sidebar.js"></script>
    <script src="<?= base_url(); ?>assets/js/lib/preloader/pace.min.js"></script>
    <!-- sidebar -->
    
    <!-- bootstrap -->

    <script src="<?= base_url(); ?>assets/js/lib/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/scripts.js"></script>
    <!-- scripit init-->
    <script src="<?= base_url(); ?>assets/js/lib/data-table/datatables.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/lib/data-table/jszip.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="<?= base_url(); ?>assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/lib/data-table/datatables-init.js"></script>

</body>


</html>