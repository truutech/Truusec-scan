<?php 

	$danger = $this->session->flashdata('danger');
	$warning = $this->session->flashdata('warning');
	$success = $this->session->flashdata('success');

	if ($danger) {
?>

<div class="alert alert-icon alert-danger alert-msg text-danger alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    <i class="mdi mdi-block-helper mr-2"></i>
    <strong>Warning!</strong> <?= $danger['message']; ?>
</div>
 <?php } ?>

 <?php if ($warning) { ?>

<div class="alert alert-icon alert-warning alert-msg text-warning alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    <i class="mdi mdi-alert mr-2"></i>
        <strong>Warning!</strong> <?= $warning['message']; ?>
</div>
 <?php } ?>

 <?php if ($success) { ?>

<div class="alert alert-success text-success alert-msg alert-dismissible fade show" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
    <i class="mdi mdi-check-all mr-2"></i>
    <strong>Success!</strong> <?= $success['message']; ?>
</div>
 <?php } ?>