<?php include_once('common/header.php'); ?>
  <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 p-r-0 title-margin-right">
              <div class="page-header">
                <div class="page-title">
                  <h1>Hello, <span>Welcome Here</span></h1>
                </div>
              </div>
            </div>
            <!-- /# column -->
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 p-l-0 title-margin-left">
              <div class="page-header page_header_2">
                <div class="page-title">
                  <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item active">home</li>
                  </ol>
                </div>
              </div>
            </div>
            <!-- /# column -->
          </div>
                <!-- /# row -->
                <section id="main-content">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="card">
                                <div class="stat-widget-two">
                                    <div class="stat-content">
                                        <div class="stat-text">Total Clients</div>
                                        <div class="stat-digit">
                                            <i class="fa fa-user"></i><?= $total_accounts; ?></div>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning w-100" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                       <!--  <div class="col-lg-3">
                            <div class="card">
                                <div class="stat-widget-two">
                                    <div class="stat-content">
                                        <div class="stat-text">Total Jobs</div>
                                        <div class="stat-digit">
                                            0</div>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-primary w-100" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <!--
                        <div class="col-lg-3">
                            <div class="card">
                                <div class="stat-widget-two">
                                    <div class="stat-content">
                                        <div class="stat-text">Total Clicks </div>
                                        <div class="stat-digit">
                                            <?= $total_clicks; ?></div>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success w-100" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                          -->
                        <!-- <div class="col-lg-3">
                            <div class="card">
                                <div class="stat-widget-two">
                                    <div class="stat-content">
                                        <div class="stat-text">Total Candidates</div>
                                        <div class="stat-digit">
                                            <i class="fa fa-user"></i><?= $total_candidates; ?></div>
                                    </div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-danger w-100" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>2018 ©  mid Admin Board by <a href="#">webstrot.</a></p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
<?php include_once('common/footer.php'); ?>