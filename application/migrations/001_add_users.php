<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_users extends CI_Migration {

        public function up()
        {
                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 11,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'display_name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                        'email' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                                'default' => null,
                        ),
                        'password' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '300',
                        ),
                        'country' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '150',
                        ),
                        'referrer' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '300',
                        ),
                        'wallet_address' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '400',
                                'default' => null,
                        ),
                        'status' => array(
                                'type' => 'BOOLEAN',
                                'default' => '1',
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('users');

                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 20,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'name' => array(
                                'type' => 'VARCHAR',
                                'constraint' => '255',
                        ),
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('games');

                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 20,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'game_play_id' => array(
                                'type' => 'INT',
                                'constraint' => '20',
                        ),
                        'user_id' => array(
                                'type' => 'INT',
                                'constraint' => '11',
                        ),
                        'started_at datetime default current_timestamp',
                        'finished_at datetime default current_timestamp',
                        'played_time datetime default current_timestamp',
                        'total_win' => array(
                                'type' => 'INT',
                                'constraint' => '20',
                                'default' => 0,
                        ),
                        'total_earn' => array(
                                'type' => 'INT',
                                'constraint' => '20',
                                'default' => 0,
                        ),
                        'created_at datetime default current_timestamp',
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('login_game');

                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 20,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'user_id' => array(
                                'type' => 'INT',
                                'constraint' => '11',
                        ),
                        'my_coins' => array(
                                'type' => 'INT',
                                'constraint' => '11',
                                'default' => 0,
                        ),
                        'created_at datetime default current_timestamp',
                        'updated_at datetime default current_timestamp',
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('coins');

                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 20,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'user_id' => array(
                                'type' => 'INT',
                                'constraint' => '11',
                        ),
                        'game_play_id' => array(
                                'type' => 'INT',
                                'constraint' => '20',
                        ),
                        'earn_coins' => array(
                                'type' => 'INT',
                                'constraint' => '20',
                                'default' => 0,
                        ),
                        'created_at datetime default current_timestamp',
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('coins_log');


                $this->dbforge->add_field(array(
                        'id' => array(
                                'type' => 'INT',
                                'constraint' => 20,
                                'unsigned' => TRUE,
                                'auto_increment' => TRUE
                        ),
                        'game_id' => array(
                                'type' => 'INT',
                                'constraint' => '20',
                        ),
                        'earn_coins' => array(
                                'type' => 'INT',
                                'constraint' => '20',
                                'default' => 0,
                        ),
                        'current_users' => array(
                                'type' => 'INT',
                                'constraint' => '11',
                                'default' => 0,
                        ),
                        'created_at datetime default current_timestamp',
                ));
                $this->dbforge->add_key('id', TRUE);
                $this->dbforge->create_table('game_play');
        }

        public function down()
        {
                $this->dbforge->drop_table('users');
                $this->dbforge->drop_table('games');
                $this->dbforge->drop_table('login_game');
                $this->dbforge->drop_table('coins');
                $this->dbforge->drop_table('coins_log');
                $this->dbforge->drop_table('game_play');
        }
}