<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();   
		$this->load->model('Common_Model');

	}
	
	public function index()
	{
		$this->load->view('login');		
	}
	
	public function login() {
		$condition = array(
			'email' => $this->input->post('email'),
			'password' => md5($this->input->post('password')),
			'status' => 1
		);
		$user = $this->db->select('*')->from('db_users')->where($condition)->get()->row();
		if ($user) {
			$this->session->set_userdata('islogin', true);
			$this->session->set_userdata('user', $user);
			$this->session->set_userdata('user_id', $user->id);
			$this->session->set_userdata('role_id', $user->role_id);
			$this->session->set_userdata('first_name', $user->first_name);
			$this->session->set_userdata('last_name', $user->last_name);
			if ($user->role_id == 2)
			redirect('Dashboard');
			else
			redirect('Site_Dashboard');
		} else {
			$this->session->set_flashdata('danger', array('message' => 'Invalid login, please try again with correct credentials.'));
			$this->load->view('login');
		}

	}
	
	public function register_form() {
		exit;
		$this->load->view('registration');
	}

	/*public function register() {
		$data = array(
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'password' => md5($this->input->post('password')),
			'status' => 1
		);
		if ($this->Common_Model->insert($data,'j_users')) {
			$user_id = $this->db->insert_id();
			$this->Common_Model->insert(array('user_id' => $user_id), 'j_payment_methods');
			$con['conditions'] = array('status' => 1);
			$aggregators = $this->Common_Model->getRows($con, 'j_aggregators');
			foreach ($aggregators as $aggregator) {
				$data = array(
					'aggregator_id' => $aggregator['id'],
					'user_id' => $user_id,
					'status' => 1,
					);
				$this->Common_Model->insert($data, 'j_assign_aggregators');
			}
			$this->session->set_flashdata('success', array('message' => 'Registration success!, please login here'));
			redirect('Login');
		} else {
			$this->session->set_flashdata('danger', array('message' => 'Something went wrong, plesae try again'));
			redirect('Login/register_form');
		}
	}*/

	function logout()
	{
		//$this->mlogin->logout();
		$this->session->sess_destroy();
		redirect('Login');
	}
	
}