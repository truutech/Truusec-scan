<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Password extends CI_Controller {
	public function __construct()
	{
		parent::__construct();   
		if (!$this->session->userdata('islogin')) {
			redirect('Login');
		}
		$this->load->model('Common_Model');
	}

	public function form() {
		$this->load->view('users/change_password', ['title' => 'Change Password']);
	}

	public function update() {
    	if ($this->input->post('changePassword')) {
    			$this->form_validation->set_rules('current_password', 'Current password', 'required');
    			$this->form_validation->set_rules('password', 'New password', 'required');
	            $this->form_validation->set_rules('confirm_password', 'Password', 'required|matches[password]');


	            $con['returnType'] = 'single';
                $con['conditions'] = array(
                    'password' => md5($this->input->post('current_password')),
                    'id' => $this->session->userdata('user_id')
                );
                
           
	             if ($this->form_validation->run() == true) {
	             	$check = $this->Common_Model->getRows($con, 'db_users');
	                if($check){

	                	$condition = array(
		                    'id'=>$this->session->userdata('user_id')
		                );

	                	$userData = array(
		                    'password' => md5(strip_tags($this->input->post('password'))),
		                );
	                	$update = $this->Common_Model->update($userData, $condition, 'db_users');

	                	if ($update) {
	                		$this->session->set_flashdata('success', array('message' => 'Password updated successfully.'));
                      	  redirect('Password/form');
	                	}

	                } else {
	                    $this->session->set_flashdata('warning', array('message' => 'Wrong current password. Please try again.'));
	                    redirect('Password/form');
	                }
	            } else {
	           	//load the view
		        $this->load->view('users/change_password', ['title' => 'Change Password']);
		    }
		}
    }
}