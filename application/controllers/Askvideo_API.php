<?php
class Askvideo_API extends CI_Controller
{
	private $api_token = '';
	private $refresh_token = '';
	/*private $api_token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ik1UTkJRamhFUmpZd05VRXlRakpFUkRGRk5rSXpPRGc0T0RZMlFqWTNSamd3TURoRVFUVTROZyJ9.eyJodHRwczovL3ZpZGVvYXNrLmNvbS9sb2dpbnNfY291bnQiOjEyLCJpc3MiOiJodHRwczovL2F1dGgudmlkZW9hc2suY29tLyIsInN1YiI6ImF1dGgwfDYwYmFkZDgzMjQ0ZjQwMDA3MWYyYjM4ZiIsImF1ZCI6WyJodHRwczovL2FwaS52aWRlb2Fzay5jb20vIiwiaHR0cHM6Ly92aWRlb2Fzay5ldS5hdXRoMC5jb20vdXNlcmluZm8iXSwiaWF0IjoxNjM3Nzc3NjExLCJleHAiOjE2Mzc3Nzg1MTEsImF6cCI6InAzTW0zOGpSaWRlaFNNTU9BOTdsVHZKMjdQQ25uR0poIiwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSBlbWFpbCJ9.plVQJ1PHYmAupdX_1cf0ShcqvKWFM170t5tljYwyiz06F4dBB0oZP6ZINlLqIeIc9hMfPpVjPAwezmkUAK1il_5hVCPJXOgwEHcF4AKfrSpl7oNR2P5BE2qLOe9gNgKduZ538bAVPZh8uR1g0pHLci-g4aJz6JejVjvr_EvLPNWR6vtUk48AyYvvplcHhayiXs-dPYNqwzIckVGtX7FKNC1B0FEgVChXBT2_aVnVTzNJSeHZfmDUKzBwDvR3C4WWIl2vLjZxlmhl-j4sfkdvXH0dY7b1n09dcQ22Q2AcoYuJXrJvM6GDJkBR1aR5YgIZ7t_nbnULZqVMc_nNMomIsg'; // for test*/

	private $client_id = 'UIsGqYPKZB3yn9WXtmijXFGgtMCzlc6J';
	private $client_secret = 'kWivpLzWz5FWMPZRHEP5QWiqvTnFO38o3TwjTIwIOKK3XTYj2ZRB5zrMW9nkGNna';
	private $organization_id = 'dafea5f6-157e-40b5-ae44-2ba668d01a8e';
	private $temp_code = '3OmMvO_DdZu5x82I';
	public function __construct()
	{
		parent::__construct();
		$this->appid = "wx2992995aa7343e48";
		$this->load->library('session');
		$token = $this->db->from('videoask_token')->where('id', 1)->get()->row();
		$this->api_token = $token->token;
		$this->refresh_token = $token->refresh_token;

	}

	public function load() {
		if (@$_GET['type'] == 'callback') {
			print_r($_GET); exit;
		}
		//$token = $this->get_access_token();
		//echo '<pre>'; print_r($token); exit;
		//$this->refresh_token();
		$forms = $this->get_forms();
		//echo '<pre>'; print_r($forms); exit;
		if (@$forms->detail == 'Token has expired.') {
			$this->refresh_token();
		}
		$prepare_questions = array();
		foreach ($forms->results as $form) {
			$prepare_questions[] = $this->get_questions($form->first_question->form_id);
		}
		//echo '<pre>'; print_r($prepare_questions); exit;
		$prepare_answers = array();
		foreach ($prepare_questions as $questions) {
			foreach ($questions->questions as $question) {
				$prepare_answers[] = $this->get_answeres($question->question_id, $question->transcription);
			}
		}
		foreach ($prepare_answers as $answer) {
			$check_answere = $this->db->where('question_id', $answer->question_id)->from('videoask_questions_answers')->get()->row();
			if ($check_answere) {
				if ($answer->answers != null) {
					$this->db->where('id', $check_answere->id)->update('videoask_questions_answers', array('answers' => json_encode($answer->answers)));
				}
			} else {
				$this->db->insert('videoask_questions_answers', array('question_id' => $answer->question_id, 'question' => $answer->question, 'answers' => json_encode($answer->answers)));
			}
		}
	}

	public function get_forms() {
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://api.videoask.com/forms',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
		    'Authorization: Bearer '.$this->api_token,
		    'organization-id: '.$this->organization_id
		  ),
		));

		$response = curl_exec($curl);
		curl_close($curl);
		return json_decode($response);
	}

	public function get_questions($form_id) {
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://api.videoask.com/forms/$form_id",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
		    'Authorization: Bearer '.$this->api_token,
		    'organization-id: '.$this->organization_id
		  ),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		return json_decode($response);
	}

	public function get_answeres($question_id, $question) {

		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://api.videoask.com/questions/'.$question_id.'/answers?limit=20&offset=0',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
		    'Authorization: Bearer '.$this->api_token,
		    'organization-id: '.$this->organization_id
		  ),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		$response = json_decode($response);
		$response->answers = $response->results;
		$response->question_id = $question_id;
		$response->question = $question; 

		return $response;
	}

	public function get_organization() {
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://api.videoask.com/organizations',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
		    'Authorization: Bearer '.$this->api_token
		  ),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		var_dump($response); exit;
	}

	public function get_access_token() {
		$data = array(
			'grant_type' => 'authorization_code',
			'code' => $this->temp_code,
			'client_id' => $this->client_id,
			'client_secret' => $this->client_secret,
			'redirect_uri' => 'https://www.medebound.com/index.php/Askvideo_API/load?type=callback'
		);
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://auth.videoask.com/oauth/token',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => http_build_query($data),
		));

		$response = curl_exec($curl);
		curl_close($curl);
		return json_decode($response);
	}

	public function refresh_token() {
		$data = array(
			'grant_type' => 'refresh_token',
			'refresh_token' => $this->refresh_token,
			'client_id' => $this->client_id,
			'client_secret' => $this->client_secret,
			//'scope' => 'openid+profile+email+offline_access'
		);
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 'https://auth.videoask.com/oauth/token',
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS => http_build_query($data),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		$response = json_decode($response);
		$this->api_token = $response->access_token;
		$this->db->where('id', 1);
    	$this->db->update('videoask_token', array('token' => $response->access_token));
		return true;
	}

}
?>
