<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class API extends CI_Controller {

	private $method;
	public function __construct()
	{
		parent::__construct();   
		$this->method = strtolower($this->input->server('REQUEST_METHOD'));
		$this->load->model('Common_Model');

	}

	public function login() {
		$condition = array(
			'email' => $this->input->post('email'),
			'password' => md5($this->input->post('password')),
		);
		$user = $this->db->select('*')->from('users')->where($condition)->get()->row();
		if ($user) {
			$this->session->set_userdata('islogin', true);
			$this->session->set_userdata('user', $user);
			$this->session->set_userdata('user_id', $user->id);
			$this->session->set_userdata('email', $user->email);
			$this->session->set_userdata('display_name', $user->display_name);

			$user = array('id' => $user->id, 'email' => $user->email, 'display_name' => $user->display_name, 'country' => $user->country, 'referrer' => $user->referrer, 'wallet_address' => $user->wallet_address);
			echo json_encode(array('success' => 1, 'details' => $user,  'msg' => 'Login successfull'));
			exit;
		} else {
			echo json_encode(array('success' => 0, 'msg' => 'Invalid login.'));
			exit;
		}

	}

	public function signup() {
		$this->form_validation->set_rules('display_name', 'Display Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() == true) {
	

		    $userData = array(
			    'display_name' => $this->input->post('display_name'),
			    'email' => $this->input->post('email'),
			    'password' => md5($this->input->post('password')),
			    'country' => $this->input->post('country'),
			    'referrer' => $this->input->post('referrer'),
			    'wallet_address' => $this->input->post('wallet_address'),
			    'wallet_address' => $this->input->post('wallet_address'),
			    'status' => 1,
			);
		    $this->Common_Model->insert($userData, 'users');
		    echo json_encode(array('success' => 1, 'Singup successfull!'));
		    exit;
		} else {
			echo json_encode(array('success' => 0, 'msg' => validation_errors()));
			exit;
		}
	}


	public function coins() {
		if ($this->method == 'get') {
			$con['conditions'] = array('user_id' => $_GET['user_id']);
			$coins = $this->Common_Model->getRows($con, 'coins');
			echo json_encode(array('success' => 1, 'details' => $coins));
		    exit;
		} elseif ($this->method == 'post') {
			$this->form_validation->set_rules('user_id', 'User ID', 'required');
			if ($this->form_validation->run() == true) {
		

			    $userData = array(
				    'user_id' => $this->input->post('user_id'),
				    'my_coins' => $this->input->post('coins'),
				);
			    $update = $this->Common_Model->insert($userData, 'coins');
			    echo json_encode(array('success' => 1, 'msg' => 'Coins created successfully!'));
			    exit;
			} else {
				echo json_encode(array('success' => 0, 'msg' => validation_errors()));
				exit;
			}
		} elseif ($this->method == 'put') {
			$this->db->set('my_coins', 'my_coins+'.$this->input->input_stream('coins'), FALSE);
			$this->db->set('updated_at', date('Y-m-d H:i:s'));
			$this->db->where('user_id', $this->input->input_stream('user_id'));
			$this->db->update('coins');
			if ($this->db->affected_rows()) {
				echo json_encode(array('success' => 1, 'msg' => 'Coins updated successfully!'));
			} else {
				echo json_encode(array('success' => 0, 'msg' => 'Something went wrong, please try again'));
			}
			
			 exit;
		}
		
	}
	
	public function games() {
		if ($this->method == 'get') {
			$games = $this->Common_Model->getRows(array(), 'games');
			echo json_encode(array('success' => 1, 'details' => $games));
		    exit;
		} elseif ($this->method == 'post') {
			$this->form_validation->set_rules('name', 'Game name', 'required');
			if ($this->form_validation->run() == true) {	
			    $userData = array(
				    'name' => $this->input->post('name'),
				);
			    $id = $this->Common_Model->insert($userData, 'games');
			    echo json_encode(array('success' => 1, 'msg' => 'Game created successfully!', 'id' => $id));
			    exit;
			} else {
				echo json_encode(array('success' => 0, 'msg' => validation_errors()));
				exit;
			}
		} elseif ($this->method == 'put') {
			$this->db->set('name', $this->input->input_stream('name'));
			$this->db->where('id', $this->input->input_stream('id'));
			$this->db->update('games');
			if ($this->db->affected_rows()) {
				echo json_encode(array('success' => 1, 'msg' => 'Game updated successfully!'));
			} else {
				echo json_encode(array('success' => 0, 'msg' => 'Something went wrong, please try again'));
			}
			
			 exit;
		}
		
	}


	public function game_play() {
		if ($this->method == 'get') {
			$games = $this->Common_Model->getRows(array(), 'game_play');
			echo json_encode(array('success' => 1, 'details' => $games));
		    exit;
		} elseif ($this->method == 'post') {
			$this->form_validation->set_rules('game_id', 'Game ID', 'required');
			$this->form_validation->set_rules('earn_coins', 'Earn Coins', 'required');
			$this->form_validation->set_rules('current_users', 'Current Users', 'required');
			if ($this->form_validation->run() == true) {	
			    $userData = array(
				    'game_id' => $this->input->post('game_id'),
				    'earn_coins' => $this->input->post('earn_coins'),
				    'current_users' => $this->input->post('current_users'),
				);
			    $this->Common_Model->insert($userData, 'game_play');
			    echo json_encode(array('success' => 1, 'msg' => 'Game Play created successfully!'));
			    exit;
			} else {
				echo json_encode(array('success' => 0, 'msg' => validation_errors()));
				exit;
			}
		} elseif ($this->method == 'put') {
			$this->db->set('earn_coins', 'earn_coins+'.$this->input->input_stream('coins'), FALSE);
			$this->db->where('id', $this->input->input_stream('id'));
			$this->db->update('game_play');
			if ($this->db->affected_rows()) {
				echo json_encode(array('success' => 1, 'msg' => 'Game play updated successfully!'));
			} else {
				echo json_encode(array('success' => 0, 'msg' => 'Something went wrong, please try again'));
			}
			    exit;
		}
		
	}


	public function game_login() {
		if ($this->method == 'get') {
			$games = $this->Common_Model->getRows(array(), 'login_game');
			echo json_encode(array('success' => 1, 'details' => $games));
		    exit;
		} elseif ($this->method == 'post') {
			$this->form_validation->set_rules('gameplay_id', 'Game Play ID', 'required');
			$this->form_validation->set_rules('user_id', 'User ID', 'required');
			$this->form_validation->set_rules('started_at', 'Started At', 'required');
			$this->form_validation->set_rules('finished_at', 'Finished At', 'required');
			$this->form_validation->set_rules('played_at', 'Played At', 'required');
			$this->form_validation->set_rules('total_win', 'Total Win', 'required');
			$this->form_validation->set_rules('earn', 'Total Earn', 'required');
			if ($this->form_validation->run() == true) {	
			    $userData = array(
				    'game_play_id' => $this->input->post('gameplay_id'),
				    'user_id' => $this->input->post('user_id'),
				    'started_at' => $this->input->post('started_at'),
				    'finished_at' => $this->input->post('finished_at'),
				    'played_time' => $this->input->post('played_at'),
				    'total_win' => $this->input->post('total_win'),
				    'total_earn' => $this->input->post('earn'),
				);
			    $id = $this->Common_Model->insert($userData, 'login_game');

			    $userData = array(
				    'user_id' => $this->input->post('user_id'),
				    'game_play_id' => $id,
				    'earn_coins' => $this->input->post('earn'),
				    'created_at' => date('Y-m-d H:i:s'),
				);
			    $this->Common_Model->insert($userData, 'coins_log');
			    echo json_encode(array('success' => 1, 'msg' => 'Game Login created successfully!', 'id' => $id));
			    exit;
			} else {
				echo json_encode(array('success' => 0, 'msg' => validation_errors()));
				exit;
			}
		} elseif ($this->method == 'put') {
			$this->db->set('total_win', 'total_win+1', FALSE);
			$this->db->set('total_earn', 'total_earn+'.$this->input->input_stream('earn'), FALSE);
			$this->db->where('game_play_id', $this->input->input_stream('gameplay_id'));
			$this->db->where('user_id', $this->input->input_stream('user_id'));
			$this->db->update('login_game');


			if ($this->db->affected_rows()) {
				$userData = array(
				    'user_id' => $this->input->post('user_id'),
				    'game_play_id' => $id,
				    'earn_coins' => $this->input->post('earn'),
				    'created_at' => date('Y-m-d H:i:s'),
				);
			    $this->Common_Model->insert($userData, 'coins_log');

				echo json_encode(array('success' => 1, 'msg' => 'Game login updated successfully!'));
			} else {
				echo json_encode(array('success' => 0, 'msg' => 'Something went wrong, please try again'));
			}
			    exit;
		}
		
	}

	public function coins_log() {
		if ($this->method == 'get') {
			$coins = $this->Common_Model->getRows(array('user_id' => $_GET['user_id']), 'coins_log');
			echo json_encode(array('success' => 1, 'details' => $coins));
		    exit;
		} elseif ($this->method == 'post') {
			$this->form_validation->set_rules('user_id', 'User ID', 'required');
			$this->form_validation->set_rules('gameplay_id', 'Game Play ID', 'required');
			$this->form_validation->set_rules('earn_coins', 'Earn Coins', 'required');
			if ($this->form_validation->run() == true) {	
			    $userData = array(
				    'user_id' => $this->input->post('user_id'),
				    'gameplay_id' => $this->input->post('gameplay_id'),
				    'earn_coins' => $this->input->post('earn_coins'),
				    'created_at' => date('Y-m-d H:i:s'),
				);
			    $this->Common_Model->update($userData, 'coins_log');
			    echo json_encode(array('success' => 1, 'msg' => 'Coins log created successfully!'));
			    exit;
			} else {
				echo json_encode(array('success' => 0, 'msg' => validation_errors()));
				exit;
			}
		} elseif ($this->method == 'put') {
		}
		
	}

	
}