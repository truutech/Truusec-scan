<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Password_Resets extends CI_Controller {

	 function __construct() {
        parent::__construct();
        $this->load->model('Common_Model');

    }

    public function index() {
    	$this->load->view('send_mail_form');
    }

    public function send_mail() {
    	if ($this->input->post('sendMail')) {

	    	$email = $this->input->post('email');
	    	$token = md5($email.time());
	    	$con['conditions'] = array(
	    		'email' => $email,
	    		'role_id' => 2
	    	);
	    	$con['returnType'] = 'single';

	    	$user = $this->Common_Model->getRows($con, 'db_users');
	    	if ($user) {
	    		$userData = array(
	    			'email' => $email,
	    			'token' => $token
	    		);
	    		$id = $this->Common_Model->insert($userData, 'db_password_resets');
	    		
	    		$userData['message'] = 'Click the below link to open your reset password form. <br /><a href="' . base_url() . 'Password_Resets/reset_pass_form/' . $token . '">Reset</a>';
	    		$userData['subject'] = 'Password reset';
	    		$result = $this->sendMail($userData);
	    		if ($result) {
	    			$this->session->set_flashdata('success', array('message' => 'We sent an email address for reset password.'));
	    			redirect('Password_Resets');
	    		} else {
	    			$this->session->set_flashdata('danger', array('message' => 'Something went wrong. Please try again.'));
	    			redirect('Password_Resets/');
	    		}
	    	} else {
	    		$this->session->set_flashdata('danger', array('message' => 'Email is not exist.'));
	    		redirect('Password_Resets');
	    	}
	    }
    }

    public function reset_pass_form($token) {
    	$data['token'] = $token;
    	$this->load->view('reset_password_form', $data);

    }

    public function change_pass() {
    	if ($this->input->post('changePassword')) {
    		$this->form_validation->set_rules('password', 'New password', 'required');
	        $this->form_validation->set_rules('confirm_password', 'Password', 'required|matches[password]');

	        $token = $this->input->post('token');
	    	$password = $this->input->post('password');
	    	$confirmPassword = $this->input->post('confirm_password');

	        if ($this->form_validation->run() == true) {

	    		$con['conditions'] = array(
	    			'token' => $token
	    		);
	    		$con['returnType'] = 'single';

	    		$token = $this->Common_Model->getRows($con, 'db_password_resets');
	    		if ($token) {
	    			$userData = array(
	    				'password' => md5($password)
	    			);
	    			$condition = array(
	    				'email' => $token['email']
	    			);

	    			$update = $this->Common_Model->update($userData, $condition, 'db_users');
	    			if ($update) {
	    				$this->Common_Model->delete($condition, 'db_password_resets');
	    				$this->session->set_flashdata('success', array('message' => 'Password changed successfully. '));
	    				redirect('Login');
	    			} else {
	    				$this->session->set_flashdata('danger', array('message' => 'Something went wrong. Please try again.'));
	    				redirect('Password_Resets/reset_pass_form/' . $token);
	    			}


	    		} else {
	    			$this->session->set_flashdata('danger', array('message' => 'Token miss match. Please try again.'));
	    			redirect('Login/');
	    		}	
	    	} else {
	    		$this->session->set_flashdata('danger', array('message' => 'Something went wrong. Password fields are rquired.'));
	    		$data['token'] = $token;
	    		$this->load->view('reset_password_form', $data);
	    	}
	    } else {
	    		redirect('Login');
	    }
	}

	function sendMail($userData)
    {
        $this->load->library('encryption');
        $this->load->library('email');
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'mail.productrvw.com'; 
        $config['smtp_port'] = '25'; // 25
        $config['smtp_timeout'] = '30';
        $config['smtp_user'] = 'info@productrvw.com'; // sales@waakhla.com
        $config['smtp_pass'] = 'i6@vr*[;0Osx'; // @mTsvpbaUV/5g
        $config['charset'] = 'utf-8';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n";

        
        $this->email->initialize($config);
        $this->email->from('info@productrvw.com', 'Drive Free'); // change it to yours
        $this->email->to($userData['email']);// change it to yours
        $this->email->subject($userData['subject']);
        $this->email->message($userData['message']);
        if($this->email->send())
        {
            return true;
        }
        else
        {
            return false;
        }

    }

   
} 
