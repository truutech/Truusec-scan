<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	private $method;
	private $api_key = 'd05486f3-4fc6-4aa7-8a94-d6e10859edcc';
	public function __construct()
	{
		parent::__construct();   
		$this->method = strtolower($this->input->server('REQUEST_METHOD'));
		$this->load->model('Common_Model');

	}

	public function index() {
		$this->load->view('home');
	}

	public function call_api() {

		//echo '<pre>'; print_r($_GET); exit;
		$url = '';
		$params = array();
		if ($_GET['fileType'] == 'moven') {
			$url = $_GET['fileType'].'/'.$_GET['groupId'].'/'.$_GET['artifactId'].'/'.$_GET['version'];
			if (isset($_GET['org'])) 
				$params['org'] = $_GET['org'];
			if (isset($_GET['repository']))
				$params['repository'] = $_GET['repository'];
		}	

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://snyk.io/api/v1/test/".$url."?".http_build_query($params));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		  "Content-Type: application/json",
		  "Authorization: token ".$this->api_key
		));

		$response = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($response);
		

	/*		$data = '{
  "ok": false,
  "issues": {
    "vulnerabilities": [
      {
        "id": "SNYK-JAVA-ORGAPACHEFLEXBLAZEDS-31455",
        "url": "https://snyk.io/vuln/SNYK-JAVA-ORGAPACHEFLEXBLAZEDS-31455",
        "title": "Arbitrary Code Execution",
        "type": "vuln",
        "description": "## Overview\n\n[org.apache.flex.blazeds:blazeds](https://github.com/apache/flex-blazeds) is an application development framework for easily building Flash-based applications for mobile devices, web browsers, and desktops.\n\n\nAffected versions of this package are vulnerable to Arbitrary Code Execution.\nThe AMF deserialization implementation of Flex BlazeDS is vulnerable to Deserialization of Untrusted Data. By sending a specially crafted AMF message, it is possible to make the server establish a connection to an endpoint specified in the message and request an RMI remote object from that endpoint. This can result in the execution of arbitrary code on the server via Java deserialization.\r\n\r\nStarting with BlazeDS version `4.7.3`, Deserialization of XML is disabled completely per default, while the `ClassDeserializationValidator` allows deserialization of whitelisted classes only. BlazeDS internally comes with the following whitelist:\r\n```\r\nflex.messaging.io.amf.ASObject\r\nflex.messaging.io.amf.SerializedObject\r\nflex.messaging.io.ArrayCollection\r\nflex.messaging.io.ArrayList\r\nflex.messaging.messages.AcknowledgeMessage\r\nflex.messaging.messages.AcknowledgeMessageExt\r\nflex.messaging.messages.AsyncMessage\r\nflex.messaging.messages.AsyncMessageExt\r\nflex.messaging.messages.CommandMessage\r\nflex.messaging.messages.CommandMessageExt\r\nflex.messaging.messages.ErrorMessage\r\nflex.messaging.messages.HTTPMessage\r\nflex.messaging.messages.RemotingMessage\r\nflex.messaging.messages.SOAPMessage\r\njava.lang.Boolean\r\njava.lang.Byte\r\njava.lang.Character\r\njava.lang.Double\r\njava.lang.Float\r\njava.lang.Integer\r\njava.lang.Long\r\njava.lang.Object\r\njava.lang.Short\r\njava.lang.String\r\njava.util.ArrayList\r\njava.util.Date\r\njava.util.HashMap\r\norg.w3c.dom.Document\r\n```\n\n## Remediation\n\nUpgrade `org.apache.flex.blazeds:blazeds` to version 4.7.3 or higher.\n\n\n## References\n\n- [CVE-2017-3066](https://nvd.nist.gov/vuln/detail/CVE-2017-5641)\n\n- [Github Commit](https://github.com/apache/flex-blazeds/commit/f861f0993c35e664906609cad275e45a71e2aaf1)\n\n- [Github Release Notes](https://github.com/apache/flex-blazeds/blob/master/RELEASE_NOTES)\n\n- [Securitytracker Issue](http://www.securitytracker.com/id/1038364)\n",
        "functions": [],
        "from": [
          "org.apache.flex.blazeds:blazeds@4.7.2"
        ],
        "package": "org.apache.flex.blazeds:blazeds",
        "version": "4.7.2",
        "severity": "critical",
        "exploitMaturity": "no-known-exploit",
        "language": "java",
        "packageManager": "maven",
        "semver": {
          "vulnerable": [
            "[,4.7.3)"
          ]
        },
        "publicationTime": "2017-08-09T14:17:08Z",
        "disclosureTime": "2017-04-25T21:00:00Z",
        "isUpgradable": true,
        "isPatchable": false,
        "isPinnable": false,
        "identifiers": {
          "CVE": [
            "CVE-2017-5641"
          ],
          "CWE": [
            "CWE-502"
          ]
        },
        "credit": [
          "Markus Wulftange"
        ],
        "CVSSv3": "CVSS:3.0/AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H",
        "cvssScore": 9.8,
        "patches": [],
        "upgradePath": [
          "org.apache.flex.blazeds:blazeds@4.7.3"
        ]
      }
    ],
    "licenses": []
  },
  "dependencyCount": 1,
  "org": {
    "name": "atokeneduser",
    "id": "689ce7f9-7943-4a71-b704-2ba575f01089"
  },
  "licensesPolicy": null,
  "packageManager": "maven"
}';
$response = json_decode($data);*/
//echo '<pre>'; print_r($response); exit;
		$this->load->view('home', array('data' => $response));
	}
}