<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {
    public function __construct()
    {
        parent::__construct();   
        if (!$this->session->userdata('islogin') || $this->session->userdata('role_id') == 1) {
            $this->session->set_flashdata('danger', array('message' => 'Access denied.'));
            redirect('Login');
        }
        $this->load->model('Common_Model');

    }
    
    public function index() {
        $con['selection'] = "db_users.*";
        $con['conditions'] = array(
            'db_users.id !=' => $this->session->userdata['user_id'],
            'db_users.role_id' => 1,
            );

        $con['orderBy'] = "db_users.id desc";


        $users = $this->Common_Model->getRows($con, 'db_users');
        $this->load->view('users/list.php', ['users' => $users, 'title' => 'Clients']);
    }


    public function add() {
        $this->load->view('users/add_form.php', ['title' => 'Add Client']);
    }

    public function store() {
        if ($this->input->post('submitForm')) {
                $this->form_validation->set_rules('first_name', 'First Name', 'required');
                $this->form_validation->set_rules('last_name', 'Last Name', 'required');
                $this->form_validation->set_rules('email', 'Email', 'required');
                $password = $this->randomPassword();
                $userData = array(
                    'first_name' => strip_tags($this->input->post('first_name')),
                    'last_name' => strip_tags($this->input->post('last_name')),
                    'email' => strip_tags($this->input->post('email')),
                    'address' => strip_tags($this->input->post('address')),
                    'autolender_name' => strip_tags($this->input->post('autolender_name')),
                    'autolender_account_number' => strip_tags($this->input->post('autolender_account_number')),
                    'payment_amount' => strip_tags($this->input->post('payment_amount')),
                    'weekly_deducted_amount' => strip_tags($this->input->post('weekly_deducted_amount')),
                    'deduction_status' => strip_tags($this->input->post('deduction_status')),
                    'autoloan_paid' => strip_tags($this->input->post('autoloan_paid')),
                    'account_balance' => strip_tags($this->input->post('account_balance')),
                    'new_account_balance' => strip_tags($this->input->post('new_account_balance')),
                    'password' => md5($password),
                    'role_id' => 1,
                    'status' => 1,
                ); 

                if ($this->check_record()) {
                    $this->session->set_flashdata('danger', array('message' => 'Email address already in use. Please try another one.'));
                        redirect('Users/add');
                }
                if ($this->form_validation->run() == true) {
                    $id = $this->Common_Model->insert($userData, 'db_users');

                    $userData['subject'] = 'Account access';
                    $userData['message'] = '<h2>Login Access</h2><br>
                                <p>Please find below account login and url</p><br>
                                <strong>Email: </strong>'.$user['email'].'<br>
                                <strong>Password: </strong>'.$password.'<br>
                                <strong>URL: </strong>'.base_url().'<br>';
                    $this->send_mail($userData);
                    if($id){
                        $this->session->set_flashdata('success', array('message' => 'Record created successfully.'));
                        redirect('Users');
                    }else{
                        $this->session->set_flashdata('danger', array('message' => 'Something went wrong. Please try again.'));
                        redirect('Users/add');
                    }
                } else {
                    $this->load->view('users/add_form.php', ['title' => 'Add Client']);
                }

        }
    }

    public function edit($user_id) {
        $con['conditions'] = array('id' => $user_id);
        $con['returnType'] = 'single';
        $user = $this->Common_Model->getRows($con, 'db_users');
        $this->load->view('users/edit_form.php', ['title' => 'Edit User', 'user' => $user]);
    }

    public function update() {
        
        if ($this->input->post('submitForm')) {
                $this->form_validation->set_rules('first_name', 'First Name', 'required');
                $this->form_validation->set_rules('last_name', 'Last Name', 'required');
                $this->form_validation->set_rules('email', 'Email', 'required');
                $user_id = $this->input->post('user_id');

                $userData = array(
                    'first_name' => strip_tags($this->input->post('first_name')),
                    'last_name' => strip_tags($this->input->post('last_name')),
                    'email' => strip_tags($this->input->post('email')),
                    'address' => strip_tags($this->input->post('address')),
                    'autolender_name' => strip_tags($this->input->post('autolender_name')),
                    'autolender_account_number' => strip_tags($this->input->post('autolender_account_number')),
                    'payment_amount' => strip_tags($this->input->post('payment_amount')),
                    'weekly_deducted_amount' => strip_tags($this->input->post('weekly_deducted_amount')),
                    'deduction_status' => strip_tags($this->input->post('deduction_status')),
                    'autoloan_paid' => strip_tags($this->input->post('autoloan_paid')),
                    'account_balance' => strip_tags($this->input->post('account_balance')),
                    'new_account_balance' => strip_tags($this->input->post('new_account_balance')),
                );  


                if ($this->form_validation->run() == true) {
                    $condition = array('id' => $user_id);
                    $update = $this->Common_Model->update($userData, $condition, 'db_users');
                    if($update){
                        $this->session->set_flashdata('success', array('message' => 'Record created successfully.'));
                        redirect('Users');
                    }else{
                        $this->session->set_flashdata('danger', array('message' => 'Something went wrong. Please try again.'));
                        redirect('Users/edit/'.$user_id);
                    }
                } else {
                    $this->load->view('users/edit_form.php', ['title' => 'Edit Client', 'user' => $userData]);
                }
        }
        

    }

    
    public function resend_credentails($user_id) {
        $con['conditions'] = array('id' => $user_id);
        $con['returnType'] = 'single';
        $user = $this->Common_Model->getRows($con, 'db_users');
        $password = $this->randomPassword();
        $condition = array('id' => $user_id);
        $this->Common_Model->update(array('password' => md5($password)), array('id' => $user_id), 'db_users');
        $userData['email'] = $user['email'];
        $userData['subject'] = 'Account access';
        $userData['message'] = '<h2>Login Access</h2><br>
                                <p>Please find below account login and url</p><br>
                                <strong>Email: </strong>'.$user['email'].'<br>
                                <strong>Password: </strong>'.$password.'<br>
                                <strong>URL: </strong>'.base_url().'<br>';
        if ($this->send_mail($userData)) {
            $this->session->set_flashdata('success', array('message' => 'Login credentials sent successfully.'));
        } else {
            $this->session->set_flashdata('danger', array('message' => 'Something went wrong. Please try again.'));
        }
        
        redirect('Users');

    }


    public function check_record() {
        $email = $this->input->post('email');
        $con['conditions'] = array('email' => $email);
        $user = $this->Common_Model->getRows($con, 'db_users');
        return $user;
    }


    public function send_mail($userData) {
        $this->load->library('encryption');
        $this->load->library('email');
        $config['protocol'] = 'mail';
        $config['smtp_host'] = 'mail.pazero.net'; 
        $config['smtp_port'] = 465; // 25
        $config['smtp_timeout'] = '30';
        $config['smtp_user'] = 'kali@pazero.net'; // sales@waakhla.com
        $config['smtp_pass'] = '+ltbuO#$S?+4'; // @mTsvpbaUV/5g
        $config['charset'] = 'utf-8';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n";

        
        $this->email->initialize($config);
        $this->email->from('kali@pazero.net', 'Drive Free'); // change it to yours
        $this->email->to($userData['email']);// change it to yours
        $this->email->subject($userData['subject']);
        $this->email->message($userData['message']);
        if($this->email->send())
        {
            return true;
        }
        else
        {
            return false;
        }
     }
    public function delete($id) {
        $condition = array('id' => $id);
        $this->Common_Model->delete($condition, 'db_users');
        $this->session->set_flashdata('success', array('message' => 'Record deleted successfully.'));
        redirect('Users');
    }

    public function update_status($user_id, $status) {
        $this->Common_Model->update(array('status' => $status), array('id' => $user_id), 'db_users');
        $this->session->set_flashdata('success', array('message' => 'Record updated successfully.'));
        redirect('Users');
    }

    function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    function detail() {
        $con['conditions'] = array('id' => $this->input->post('id'));
        $con['returnType'] = 'single';
        $user = $this->Common_Model->getRows($con, 'db_users');
        $html = $this->load->view('users/detail', ['user' => $user], true);
        exit($html);
    }
}