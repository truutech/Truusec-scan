 <?php  
 class Crud_model extends CI_Model  
 {  
      var $table = "j_subscribers";  
      var $select_column = array("id", "name", "email", "location", "job_title", "created", "status", "subscriber");  
      var $order_column = array(null, "name", "email", "location", "job_title", "created", null);  
      function make_query($status, $subscriber)  
      {  
           $this->db->select($this->select_column);  
           $this->db->from($this->table);  
           $this->db->where(['subscriber' => $subscriber]);
           if ($subscriber == 1) {
              $this->db->where(['status' => $status]); 
            } 
           if(isset($_POST["search"]["value"]))  
           {  
                $this->db->or_like("name", $_POST["search"]["value"]); 
                $this->db->where([ 'subscriber' => $subscriber]);
                if ($subscriber == 1) {
                  $this->db->where(['status' => $status]); 
                } 
                $this->db->or_like("email", $_POST["search"]["value"]);  
                $this->db->where([ 'subscriber' => $subscriber]);
                if ($subscriber == 1) {
                  $this->db->where(['status' => $status]); 
                } 
                $this->db->or_like("location", $_POST["search"]["value"]);  
                $this->db->where([ 'subscriber' => $subscriber]);
                if ($subscriber == 1) {
                  $this->db->where(['status' => $status]); 
                }  
                $this->db->or_like("job_title", $_POST["search"]["value"]);  
                $this->db->where([ 'subscriber' => $subscriber]);
                if ($subscriber == 1) {
                  $this->db->where(['status' => $status]); 
                }  
           }  
           if(isset($_POST["order"]))  
           {  
                $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);  
           }  
           else  
           {  
                $this->db->order_by('id', 'DESC');  
           }  
      }  
      function make_datatables($status, $subscriber){  
           $this->make_query($status, $subscriber);  
           if($_POST["length"] != -1)  
           {  
                $this->db->limit($_POST['length'], $_POST['start']);  
           }  
           $query = $this->db->get();  
           return $query->result();  
      }  
      function get_filtered_data($status, $subscriber){  
           $this->make_query($status, $subscriber);  
           $query = $this->db->get();  
           return $query->num_rows();  
      }       
      function get_all_data()  
      {  
           $this->db->select("*");  
           $this->db->from($this->table);  
           return $this->db->count_all_results();  
      }  
 }  